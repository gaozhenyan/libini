#include "libini.h"

void libini_destroy_property(ini_property_t *property)
{
    assert(property);

    if(property->key)
    {
	free(property->key);
    }

    if(property->value)
    {
	free(property->value);
    }

    free(property);
}

void libini_destroy_section(ini_section_t *section)
{
    assert(section);

    ini_property_t *property, *property_backup;
    list_for_each_entry_safe(property, property_backup, &section->property_list, list)
    {
	libini_destroy_property(property);
    }

    if(section->name)
    {
	free(section->name);
    }

    free(section);
}

void libini_destroy(ini_file_t *config_file)
{
    ini_section_t *section, *section_del;
    ini_property_t *property, *property_del;

    assert(config_file);

    list_for_each_entry_safe(section, section_del, &config_file->section_list, list)
    {
	libini_destroy_section(section);
    }

    if(config_file->path)
    {
	free(config_file->path);
    }

    free(config_file);
}

void libini_print_config(ini_file_t *file)
{
    ini_section_t *section;
    ini_property_t *property;

    list_for_each_entry(section, &file->section_list, list)
    {
	printf("%s\n", section->name);

	list_for_each_entry(property, &section->property_list, list)
	{
	    printf("\t%s = %s\n", property->key, property->value);
	}
    }
}

ini_file_t* libini_init(char *config_file)
{

    assert(config_file);

    ini_file_t *file;
    file = (ini_file_t*)malloc(sizeof(ini_file_t));
    if(!file)
    {
	syslog(LOG_DAEMON, "%s", "Can not alloc memory\n");
	return NULL;
    }

    INIT_LIST_HEAD(&file->section_list);
    INIT_LIST_HEAD(&file->property_list);
    file->path = strdup(config_file);

    if(access(config_file, F_OK) < 0)
    {
	syslog(LOG_DAEMON, "%s", "The file does not exist.\n");
	//fprintf(stderr, "%s", "The file does not exist.\n");
	return NULL;
    }

    if(access(config_file, R_OK) < 0)
    {
	syslog(LOG_DAEMON, "%s", "The file can not be read.");
	//fprintf(stdout, "%s", "The file can not be read.");
	return NULL;
    }

    FILE *fp;
    fp = fopen(config_file, "r");

    char *file_content = malloc(LIBINI_MAX_SUPPORT_FILE_SIZE);
    memset(file_content, 0, LIBINI_MAX_SUPPORT_FILE_SIZE);

    /*Try to read all the content in one try, reduce file I/O*/
    fread(file_content, LIBINI_MAX_SUPPORT_FILE_SIZE, 1, fp);
    if(ferror(fp))
    {
	syslog(LOG_DAEMON, "%s", "libini error reading file, will not parse");
	return NULL;
    }

    if(!feof(fp))
    {
	syslog(LOG_DAEMON, "%s", "The file is too big");
	return NULL;
    }


    char *current_line;
    char *saveptr;
    file->real_lines = 0;
    current_line = strtok_r(file_content, "\r\n", &saveptr);
    while(current_line)
    {
	file->lines[file->real_lines] = strdup(current_line);
	file->real_lines++;
	if(file->real_lines == LIBINI_MAX_SUPPORT_FILE_LINES)
	{
	    break;
	}
	current_line = strtok_r(NULL, "\r\n", &saveptr);
    }

    ini_section_t *current_section;
    ini_property_t *current_property;

    int index;
    char *section_name;
    char *token;
    char *saveptr_section;
    char *saveptr_property;

    for(index = 0; index < file->real_lines; index++)
    {
	current_line = file->lines[index];
	
	if(*current_line == ';' || *current_line == '#')
	{
	    continue;
	}

	if (strchr(current_line, ']'))
	{
	    current_section = (ini_section_t*)malloc(sizeof(ini_section_t));
	    memset(current_section, 0, sizeof(ini_section_t));
	    list_add_tail(&current_section->list, &file->section_list);

	    INIT_LIST_HEAD(&current_section->property_list);
	    section_name = strtok_r(current_line, "[]", &saveptr_section);
	    current_section->name = strdup(section_name);
	}

	if(strchr(current_line, '='))
	{
	    current_property = (ini_property_t*)malloc(sizeof(ini_property_t));
	    memset(current_property, 0, sizeof(ini_property_t));
	    list_add_tail(&current_property->list, &current_section->property_list);
	    current_property->my_section = current_section;
	    token = strtok_r(current_line, "=", &saveptr_property);
	    current_property->key = strdup(token);
	    token = strtok_r(NULL, "=", &saveptr_property);
	    current_property->value = strdup(token);
	}

    }

    free(file_content);
    for(index = 0; index< file->real_lines; index++)
    {
	free(file->lines[index]);
    }
    fclose(fp);
    return file;

} 

ini_section_t* libini_find_section(ini_file_t *config_file, 
	char *section_name)
{
    ini_section_t *cursor;
    list_for_each_entry(cursor, &config_file->section_list, list)
    {
	if(!strcmp(cursor->name,section_name))
	{
	    return cursor;
	}
    }

    syslog(LOG_DAEMON, "%s", "The requested section does NOT exist.");
    return NULL;
}

ini_property_t* libini_find_property(ini_file_t *config_file, 
	char *section_name, char *property_name)
{
    ini_section_t *section;
    ini_property_t *property;
    section = libini_find_section(config_file, section_name);
    if(!section)
    {
	//fprintf(stderr, "%s\n", "section does not exist");
	return NULL;
    }

    list_for_each_entry(property, &section->property_list, list)
    {
	if(!strcmp(property->key,property_name))
	{
	    return property;
	}
    }

    syslog(LOG_DAEMON, "%s", "The requested property does NOT exist.");
    return NULL;
}

int libini_is_section_exist(ini_file_t *config_file, char *section_name)
{
    return libini_find_section(config_file, section_name) ? 1 : 0;
}

int libini_get_value(ini_file_t *config_file, char *section_name,char *key,
	                        char *buf, size_t buflen)
{
    ini_section_t *section;
    section = libini_find_section(config_file, section_name);
    if(!section)
    {
	//fprintf(stderr, "%s\n", "section not found");
	return -1;
    }

    ini_property_t *property;
    list_for_each_entry(property, &section->property_list, list)
    {
	if(!strcmp(property->key, key))
	{
	    strncpy(buf, property->value, buflen);
	    return 0;
	}
    }

    //fprintf(stderr, "%s\n", "the key does not exist");
    return -1;
}

int libini_set_value(ini_file_t *config_file, char *section_name,
	                        char *key, char *value)
{
    ini_section_t *section;
    section = libini_find_section(config_file, section_name);
    if(!section)
    {
	syslog(LOG_DAEMON, "the requested section %s does not exist, \
		create a new one for it", section_name);
	section = (ini_section_t*)malloc(sizeof(ini_section_t));
	INIT_LIST_HEAD(&section->property_list);
	section->name = strdup(section_name);
	section->my_file = config_file;
	list_add_tail(&section->list, &config_file->section_list);
    }

    ini_property_t *property;
    list_for_each_entry(property, &section->property_list, list)
    {
	if(!strcmp(property->key, key))
	{
	    if(property->value)
	    {
		free(property->value);
		property->value = strdup(value);
	    }
	    return 0;
	}
    }

    syslog(LOG_DAEMON, "%s", "the key does not exist, create a new one");
    property = (ini_property_t*)malloc(sizeof(ini_property_t));
    property->my_section = section;
    property->key = strdup(key);
    property->value = strdup(value);
    list_add_tail(&property->list, &section->property_list);

    return 0;
}

int libini_remove_property(ini_file_t *config_file, char *section_name,
	                        char *key)
{
    ini_property_t *property;
    
    if( (property = libini_find_property(config_file, section_name, key)) );
    {
	list_del(&property->list);
	libini_destroy_property(property);
    }

    return 0;
}

int libini_remove_section(ini_file_t *file, char *section_name)
{
    ini_section_t *section;

    if(!(section = libini_find_section(file, section_name)))
    {
	syslog(LOG_DAEMON, "%s", "The requested section doesn't exist");
	//fprintf(stderr, "%s\n", "section doesn't exist");
	return -1;
    }

    list_del(&section->list);
    libini_destroy_section(section);
    return 0;
}


int libini_save(ini_file_t *config_file)
{
    ini_section_t *section;
    ini_property_t *property;

    FILE *save_fp;

    int is_empty = 0;

    if(list_empty_careful(&config_file->section_list))
    {
	is_empty = 1;
    }

    if(access(config_file->path, W_OK)<0)
    {
	printf("access\n");
	return -1;
    }

    save_fp = fopen(config_file->path, "w+");
    if(!save_fp)
    {
	printf("fopen\n");
	return -1;
    }

    if(is_empty)
    {
	fclose(save_fp);
	return 0;
    }

    list_for_each_entry(section, &config_file->section_list, list)
    {
	fputc('[', save_fp);
	fputs(section->name, save_fp);
	fputc(']', save_fp);
	fputc('\n', save_fp);
	if(list_empty(&section->property_list))
	{
	    continue;
	}
	list_for_each_entry(property, &section->property_list, list)
	{
	    fputs(property->key, save_fp);
	    fputc('=', save_fp);
	    fputs(property->value, save_fp);
	    fputc('\n', save_fp);
	}

	fputc('\n', save_fp);
    }

    fflush(save_fp);
    fclose(save_fp);
    return 0;
}
