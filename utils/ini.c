#include "libini.h"

void usage(void)
{
    printf("usage:\n"
	   "\tini read file section key\n"
	   "\tini write file section key value\n"
	   "\tini del_property file section key\n"
	   "\tini del_section file section\n");
}

int main(int argc, char *argv[])
{
    char *value;
    int read_flag = 0;
    int write_flag = 0;
    int del_property = 0;
    int del_section = 0;
    
    if(argc == 1)
    {
	usage();
	return 0;
    }

    if(!strcmp("read", argv[1]))
    {
	if(argc != 5)
	{
	    fprintf(stderr, "%s\n", "Paramter Error");
	    return -1;
	}
	read_flag = 1;
    }
    else if(!strcmp("write", argv[1]))
    {
	if(argc != 6)
	{
	    fprintf(stderr, "%s\n", "Paramter Error");
	    return -1;
	}
	write_flag = 1;
	value = strdup(argv[5]);
    }
    else if(!strcmp("del_property", argv[1]))
    {
	if(argc != 5)
	{
	    fprintf(stderr, "%s\n", "Paramter Error");
	    return -1;
	}
	del_property = 1;
    }
    else if(!strcmp("del_section", argv[1]))
    {
	if(argc != 4)
	{
	    fprintf(stderr, "%s\n", "Paramter Error");
	    return -1;
	}
	del_section = 1;
    }
    else
    {
	usage();
	return -1;
    }
	

    char *file = strdup(argv[2]);
    char *section = strdup(argv[3]);
    char *key;

    if(argc >= 5)
    {
	key = strdup(argv[4]);
    }

    ini_file_t *configfile;
    configfile = libini_init(file);
    if(!configfile)
    {
	fprintf(stderr, "%s\n", "Can not ini");
	return -1;
    }

    char buf[128];
    memset(buf, 0, sizeof(buf));
    if(read_flag)
    {
	libini_get_value(configfile, section, key, buf, sizeof(buf));
	printf("%s\n", buf);
    }
    else if(write_flag)
    {
	libini_set_value(configfile, section, key, value);
	libini_save(configfile);
	libini_destroy(configfile);
    }
    else if(del_property)
    {
	libini_remove_property(configfile, section, key);
	libini_save(configfile);
	libini_destroy(configfile);
    }
    else if(del_section)
    {
	libini_remove_section(configfile, section);
	libini_save(configfile);
	libini_destroy(configfile);
    }


    exit(EXIT_SUCCESS);

}
