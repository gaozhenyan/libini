#ifndef LIBINI_H
#define LIBINI_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <syslog.h>
#include "libini_list.h"

/*
 * We will read up to LIBINI_MAX_SUPPORT_FILE_SIZE bytes
 * from file and parse up to LIBINI_MAX_SUPPORT_FILE_LINES lines
 * in the file.
 * */
#define LIBINI_MAX_SUPPORT_FILE_SIZE (1024)
#define LIBINI_MAX_SUPPORT_FILE_LINES (128)

typedef struct config_file 
{
    struct list_head section_list;
    struct list_head property_list;
    char *path;
    char *lines[LIBINI_MAX_SUPPORT_FILE_LINES];
    int real_lines;
} ini_file_t;

typedef struct section 
{
    char *name;
    ini_file_t *my_file;
    struct list_head property_list;
    struct list_head list;
} ini_section_t;

typedef struct property
{
    char *key;
    char *value;
    ini_section_t* my_section;
    struct list_head list;
} ini_property_t;

ini_file_t* libini_init(char *config_file);
ini_section_t* libini_find_section(ini_file_t *config_file, char *section_name);
int libini_is_section_exist(ini_file_t *config_file, char *section_name);
int libini_get_value(ini_file_t *config_file, char *section_name,
	                     char *key, char *buf, size_t buflen);
int libini_set_value(ini_file_t *config_file, char *section_name,
	                     char *key, char *value);
int libini_save(ini_file_t *config_file);
int libini_remove_section(ini_file_t *file, char *section_name);
int libini_remove_property(ini_file_t *config_file, char *section_name, char *key);
void libini_destroy_property(ini_property_t *property);
void libini_destroy_section(ini_section_t *section);
void libini_destroy(ini_file_t *config_file);
void libini_print_config(ini_file_t *file);






#endif
